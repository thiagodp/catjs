# Requisitos do Produto

- A descrição deve ter no mínimo 2 caracteres.
- A descrição deve ter no máximo 100 caracteres.
- A quantidade deve ser no mínimo zero (0).
- A quantidade deve ser no máximo 9.999.
- O preço deve ser de no mínimo 0.01 (um centavo).
- O preço deve ser de no máximo 999.999.99.
- Ao ser cadastrado com sucesso, deve exibir a mensagem "Salvo com sucesso.".