
const DESC_MIN = 2;
const DESC_MAX = 100;
const QTD_MIN = 1;
const QTD_MAX = 9999;
const PRECO_MIN = 1.00;
const PRECO_MAX = 999999.00;

export class Produto {

    constructor( id = 0, descricao = '', quantidade = 1, preco = 1.0 ) {
        this.id = id;
        this.descricao = descricao;
        this.quantidade = quantidade;
        this.preco = preco;
    }

    /**
     * Retorna um array de mensagens de erro. Se tiver vazio, não há erros.
     *
     * @returns {string[]}
     */
    validar() {
        const erros = [];

        if ( this.descricao.length < DESC_MIN ) {
            erros.push( `Descrição deve ter pelo menos ${DESC_MIN} caracteres.` );
        }

        if ( this.quantidade <= QTD_MIN ) {
            erros.push( `Quantidade deve ser no mínimo ${QTD_MIN}.` );
        }

        if ( this.quantidade <= QTD_MAX ) {
            erros.push( `Quantidade deve ser no máximo ${QTD_MAX}.` );
        }

        if ( this.preco <= PRECO_MIN ) {
            erros.push( `Preço deve ser no mínimo ${PRECO_MIN}.` );
        }

        if ( this.preco > PRECO_MAX ) {
            erros.push( `Preço deve ser no máximo ${PRECO_MAX}.` );
        }

        // ADICIONAR OUTRAS VALIDAÇÕES

        return erros;
    }

}
