// Resultado: 'X', 'O', 'E', ''

export const RESULTADO_VENCEDOR_X = 'X';
export const RESULTADO_VENCEDOR_O = 'O';
export const RESULTADO_EMPATE = 'E';
export const RESULTADO_SEM_VENCEDOR = '';

/**
 * Jogo
 *
 * @author Thiago Delgado Pinto
 */
export class Game {

    _vez = 'X';

    _jogadas = [ '', '', '', '', '', '', '', '', '' ];

    _resultado = '';

    reiniciar() {
        this._vez = 'X';
        this._jogadas = [ '', '', '', '', '', '', '', '', '' ];
        this._resultado = RESULTADO_SEM_VENCEDOR;
    }

    vez() {
        return this._vez;
    }

    jogar( indice ) {
        if ( this._resultado !== RESULTADO_SEM_VENCEDOR ) {
            return false;
        }
        if ( ! this._estaOcupada( indice ) ) {
            this._jogadas[ indice ] = this.vez();
            this._resultado = this._avaliarResultado();
            this._trocarVez();
            return true;
        }
        return false;
    }

    resultado() {
        return this._resultado;
    }

    indicesVencedores() {
        const r = this.resultado();
        if ( ! ( RESULTADO_VENCEDOR_X === r || RESULTADO_VENCEDOR_O === r ) ) {
            return null;
        }
        return this._vencedor( r );
    }

    // privado

    _trocarVez() {
        this._vez = 'X' === this._vez ? 'O' : 'X';
    }

    _avaliarResultado() {
        if ( this._vencedor( 'X' ) ) {
            return RESULTADO_VENCEDOR_X;
        }
        if ( this._vencedor( 'O' ) ) {
            return RESULTADO_VENCEDOR_O;
        }
        if ( this._completo() ) {
            return RESULTADO_EMPATE;
        }
        return RESULTADO_SEM_VENCEDOR;
    }

    _jogadaEm( indice ) {
        if ( indice < 0 || indice >= this._jogadas.length ) {
            return null;
        }
        return this._jogadas[ indice ];
    }

    _estaOcupada( indice ) {
        return this._jogadaEm( indice ) != '';
    }

    _vencedor( quem ) {
        // 0 1 2
        // 3 4 5
        // 6 7 8

        // Linhas
        return this._vence( quem, 0, 1, 2 )
            || this._vence( quem, 3, 4, 5 )
            || this._vence( quem, 6, 7, 8 )
        // Colunas
            || this._vence( quem, 0, 3, 6 )
            || this._vence( quem, 1, 4, 7 )
            || this._vence( quem, 2, 5, 8 )
        // Diagonais
            || this._vence( quem, 0, 4, 8 )
            || this._vence( quem, 2, 4, 6 );
    }

    /**
     * Vence se tiver certo jogador (quem) nas posições indicas.
     *
     * @param {string} quem Jogador
     * @param  {number[]} posicoes Posições
     * @returns {number[] | null}
     */
    _vence( quem, ...posicoes ) {
        const contagem = posicoes.filter(
            i => this._jogadaEm( i ) === quem ).length;
        return 3 === contagem ? [ ...posicoes ] : null;
    }

    _totalJogadas() {
        return this._jogadas.filter( jogada => jogada != '' ).length;
    }

    _completo() {
        return this._totalJogadas() === this._jogadas.length;
    }

}