import { Game, RESULTADO_SEM_VENCEDOR, RESULTADO_EMPATE } from "../game/game.js";
import { Som } from './som.js';

/**
 * Tabuleiro do jogo
 *
 * @author Thiago Delgado Pinto
 */
export class Tabuleiro {

    constructor() {
        this._casas = Array.from( document.querySelectorAll( "div[class='casa']" ) );
        this._eventosCasas = this._casas.map( ( _, i ) => this._montarAcaoDaCasa( i ) );
        this._preparado = false;
        this._som = new Som();
        this._game = new Game();
    }

    configurar() {
        this._casas.forEach( ( v, i ) => v.addEventListener( 'click', this._eventosCasas[ i ] ) );
    }

    desconfigurar() {
        this._casas.forEach( ( v, i ) => v.removeEventListener( 'click', this._eventosCasas[ i ] ) );
    }

    preparar() {
        this._game.reiniciar();
        this._limparCasas();
        this._indicarVez( this._game.vez() );
        this._preparado = true;
        this._som.inicio();
    }

    desenharCasa( indice, valor ) {
        if ( indice < 0 || index >= this._casas.length ) {
            return;
        }
        this._casas[ indice ].textContent = valor;
    }

    indicarSituacao( situacao ) {
        document.getElementById( 'situacao' ).innerHTML = situacao;
    }

    // privado

    _montarAcaoDaCasa( indice ) {

        const _thisOfClass = this;

        // Retorna uma função que será usada no clique de cada casa
        return function() {
            // Jogo não preparado para começar?
            if ( ! _thisOfClass._preparado ) {
                return;
            }
            // this.textContent = index;
            const vezAntesDeJogar = _thisOfClass._game.vez();
            const ok = _thisOfClass._game.jogar( indice );
            if ( ! ok ) {
                return;
            }
            _thisOfClass._som.jogada(); // Reproduz o som
            this.textContent = vezAntesDeJogar; // Mostra o caractere na casa
            // Avalia o resultado e exibe mensagem de acordo
            const resultado = _thisOfClass._game.resultado();
            if ( RESULTADO_SEM_VENCEDOR === resultado ) {
                // Indica a vez, já que ninguém venceu
                _thisOfClass._indicarVez( _thisOfClass._game.vez() );
            } else {
                if ( RESULTADO_EMPATE === resultado ) {
                    _thisOfClass.indicarSituacao( 'Empate 🤷‍♂️' );
                    _thisOfClass._som.empate();
                } else {
                    _thisOfClass.indicarSituacao( '✨ 🏆 <span class="jogador" >' + resultado + '</span> venceu 🏆 ✨' );
                    // "Pinta" as casas vencedoras
                    const indices = _thisOfClass._game.indicesVencedores();
                    indices.forEach( i => _thisOfClass._casas[ i ].classList.add( 'casaVencedora' ) );
                    // Reproduz o som
                    _thisOfClass._som.vitoria();
                }
                // Desabilita o "hover" das casas
                _thisOfClass._removerFlutuacao();
                // Indica que o jogo não está preparado (jogador precisará iniciar)
                this._preparado = false;
            }
        };
    }

    _limparCasas() {
        this._casas.forEach( b => {
            b.textContent = '';
            b.classList.remove( 'casaVencedora' );
            b.classList.add( 'casaFlutuavel' );
        } );
    }

    _indicarVez( vez ) {
        this.indicarSituacao( 'Vez de <span class="jogador" >' + vez + '</span>' );
    }

    _removerFlutuacao() {
        this._casas.forEach( c => c.classList.remove( 'casaFlutuavel' ) );
    }

}