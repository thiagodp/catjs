
// C4-C5
const DO = 60;
const RE = 62;
const MI = 64;
const FA = 65;
const SOL = 67;
const LA = 69;
const SI = 71;

const length = 2;
const eps = 0.01;

/**
 * Som para o jogo
 *
 * @author Thiago Delgado Pinto
 */
export class Som {

    constructor() {
        this._context = new AudioContext();
    }

    inicio() {
        const t = 20;
        this.tocar( [
            [ DO, t ],
            [ SOL, t ],
        ] );
    }

    jogada() {
        this.tocar( [ [ DO, 1000 ] ] );
    }

    empate() {
        const t = 70;
        this.tocar( [
            [ SI, t ],
            [ LA, t, ],
            [ SOL, t ],
            [ FA, t ],
            [ MI, t ],
            [ RE, t ],
            [ DO, t ],
        ] );
    }

    vitoria() {
        const t = 70;
        this.tocar( [
            [ DO, t ],
            [ RE, t ],
            [ MI, t ],
            [ FA, t ],
            [ SOL, t ],
            [ LA, t, ],
            [ SI, t ]
        ] );
    }

    /**
     * Toca uma matriz de notas.
     *
     * @param {*} mideNoteMatrix Matriz de tuplas com nota MIDI e tempo
     */
    tocar( mideNoteMatrix ) {

        if ( ! Array.isArray( mideNoteMatrix )
            || ! Array.isArray( mideNoteMatrix[ 0 ] ) ) {
            throw new Error( 'Deve ser um array de array, como [ [ DO, 4 ] ].' );
        }

        const context = this._context;
        context.resume();

        const oscillator = context.createOscillator();
        oscillator.connect( context.destination );
        oscillator.type = 'triangle';
        oscillator.start( 0 );

        const midiNoteToFrequency = note => Math.pow( 2, ( note - 69 ) / 12 ) * 440;

        let time = context.currentTime + eps;
        mideNoteMatrix.forEach( tuple => {
            const freq = midiNoteToFrequency( tuple[ 0 ] );
            const noteTime = tuple[ 1 ];
            oscillator.frequency.setTargetAtTime( 0, time - eps, 0.001 );
            oscillator.frequency.setTargetAtTime( freq, time, 0.001 );
            time += length / noteTime;
        } );

        oscillator.stop( time );
    }

}