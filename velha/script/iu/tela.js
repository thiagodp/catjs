import { Tabuleiro } from "./tabuleiro.js";

/**
 * Tela do jogo
 *
 * @author Thiago Delgado Pinto
 */
export class Tela {

    constructor() {
        this._iniciar = document.getElementById( 'iniciar' );
        this._tabuleiro = new Tabuleiro();
    }

    configurar() {
        this._tabuleiro.configurar();
        this._iniciar.addEventListener( 'click', this._iniciarJogo );
    }

    desconfigurar() {
        this._tabuleiro.desconfigurar();
        this._iniciar.removeEventListener( 'click', this._iniciarJogo );
    }

    // privado

    _iniciarJogo = e => {
        this._tabuleiro.preparar();
    };

}