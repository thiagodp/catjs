import { Tela } from "./iu/tela.js";

const tela = new Tela();

window.addEventListener( 'load', () => {
    tela.configurar();
} );

window.addEventListener( 'unload', () => {
    tela.desconfigurar();
} );
